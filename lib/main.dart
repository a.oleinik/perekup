import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:perekup/new_car_dialog.dart';
import 'package:perekup/providers/theme.dart';
import 'hive/car.dart';
import './pages/home.dart';

void main() async {
  await Hive.initFlutter();
  Hive.registerAdapter(CarAdapter());

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'PerekupPro',
      theme: PerekupTheme.light(context),
      home: const MainPage(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class MainPage extends StatefulWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  int currentTab = 0;

  void setTab(index) {
    setState(() {
      currentTab = index;
    });
  }

  final _pageNavigation = [
    const GaragePage(),
    const Center(
      child: Text('2'),
    ),
    const Center(
      child: Text('3'),
    ),
    const Center(
      child: Text('4'),
    )
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: IndexedStack(
        children: _pageNavigation,
        index: currentTab,
      ),
      bottomNavigationBar: _buttomNavBar(),
    );
  }

  BottomNavigationBar _buttomNavBar() {
    return BottomNavigationBar(
      elevation: 8,
      type: BottomNavigationBarType.fixed,
      showUnselectedLabels: false,
      showSelectedLabels: false,
      unselectedItemColor: Colors.grey,
      selectedItemColor: Colors.black,
      currentIndex: currentTab,
      onTap: setTab,
      items: const [
        BottomNavigationBarItem(
            icon: Icon(Icons.garage_outlined),
            label: '',
            activeIcon: Icon(Icons.garage)),
        BottomNavigationBarItem(
            icon: Icon(Icons.inventory_2_outlined),
            label: '',
            activeIcon: Icon(Icons.inventory_2)),
        BottomNavigationBarItem(
            icon: Icon(Icons.sticky_note_2_outlined),
            label: '',
            activeIcon: Icon(Icons.sticky_note_2)),
        BottomNavigationBarItem(
          icon: Icon(Icons.account_circle_outlined),
          label: '',
          activeIcon: Icon(Icons.account_circle),
        )
      ],
    );
  }
}

class CarsMainScreen extends StatelessWidget {
  const CarsMainScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: ValueListenableBuilder(
            valueListenable: Hive.box('settings').listenable(),
            builder: _buildWithBox,
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.add),
        onPressed: () {
          showDialog(
            context: context,
            builder: (context) {
              return NewCarDialog();
            },
          );
        },
      ),
    );
  }

  Widget _buildWithBox(BuildContext context, Box settings, Widget? child) {
    var reversed = settings.get('reversed', defaultValue: true) as bool;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'Hive Cars',
              style: TextStyle(fontSize: 40),
            ),
            const SizedBox(width: 20),
            IconButton(
              icon: Icon(
                reversed ? Icons.keyboard_arrow_up : Icons.keyboard_arrow_down,
                size: 32,
              ),
              onPressed: () {
                settings.put('reversed', !reversed);
              },
            ),
          ],
        ),
        const SizedBox(height: 10),
        const Text(
          'Refresh this tab to test persistence.',
          textAlign: TextAlign.center,
        ),
        const SizedBox(height: 40),
        Expanded(
          child: ValueListenableBuilder<Box<Car>>(
            valueListenable: Hive.box<Car>('cars').listenable(),
            builder: (context, box, _) {
              var todos = box.values.toList().cast<Car>();
              if (reversed) {
                todos = todos.reversed.toList();
              }
              return ListView.builder(
                  itemCount: todos.length,
                  itemBuilder: (context, index) {
                    return ListTile(
                      title: Text(todos[index].model),
                    );
                  });
            },
          ),
        ),
      ],
    );
  }
}
