import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

abstract class PerekupTheme {
  static ThemeData light =
     ThemeData(
      primarySwatch: Colors.cyan,
      
      visualDensity: VisualDensity.adaptivePlatformDensity,
    );
  }
}
