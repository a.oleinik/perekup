import 'package:hive/hive.dart';

part 'car.g.dart';

@HiveType(typeId: 0)
class Car extends HiveObject {
  @HiveField(0)
  late String model;

  @HiveField(1)
  late DateTime createdDate;

  @HiveField(2)
  late bool isSold;

  @HiveField(3)
  late int makeYear;

  @HiveField(4)
  late double price;
}
