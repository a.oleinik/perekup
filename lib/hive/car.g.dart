// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'car.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class CarAdapter extends TypeAdapter<Car> {
  @override
  final int typeId = 0;

  @override
  Car read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Car()
      ..model = fields[0] as String
      ..createdDate = fields[1] as DateTime
      ..isSold = fields[2] as bool
      ..makeYear = fields[3] as int
      ..price = fields[4] as double;
  }

  @override
  void write(BinaryWriter writer, Car obj) {
    writer
      ..writeByte(5)
      ..writeByte(0)
      ..write(obj.model)
      ..writeByte(1)
      ..write(obj.createdDate)
      ..writeByte(2)
      ..write(obj.isSold)
      ..writeByte(3)
      ..write(obj.makeYear)
      ..writeByte(4)
      ..write(obj.price);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CarAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
