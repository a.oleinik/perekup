import 'package:flutter/material.dart';

class CarItem extends StatelessWidget {
  const CarItem({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 16),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(8.0),
        boxShadow: [
          BoxShadow(
              color: Colors.grey.withOpacity(0.1),
              spreadRadius: 1,
              blurRadius: 5,
              offset: const Offset(0, 3))
        ],
      ),
      child: Column(
        children: [
          Row(
            children: const [
              Text(
                'Volkswagen',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
              ),
              SizedBox(
                width: 4,
              ),
              Text(
                'Toureg,',
                style: TextStyle(fontWeight: FontWeight.w500, fontSize: 15),
              ),
              SizedBox(
                width: 4,
              ),
              Text(
                '2012',
                style: TextStyle(fontWeight: FontWeight.w500, fontSize: 15),
              ),
              Spacer(),
              Text('21 400 \$',
                  style: TextStyle(
                      fontWeight: FontWeight.w500,
                      color: Colors.red,
                      fontSize: 16))
            ],
          ),
          const SizedBox(
            height: 8,
          ),
          Row(
            children: const [
              Text('2012'),
              SizedBox(
                width: 8,
              ),
              Text('3.0'),
              SizedBox(
                width: 8,
              ),
              Text('Disel')
            ],
          ),
          const SizedBox(
            height: 8,
          ),
          Row(
            children: [
              const Text('Расходы'),
              Container(
                decoration: BoxDecoration(
                  color: Theme.of(context).primaryColor,
                  borderRadius: BorderRadius.circular(8.0),
                ),
                margin: const EdgeInsets.only(left: 4),
                padding: const EdgeInsets.symmetric(horizontal: 6, vertical: 2),
                child: const Text('2300 \$'),
              )
            ],
          )
        ],
      ),
    );
  }
}
