import 'package:flutter/material.dart';
import 'package:perekup/widgets/car_item.dart';

class CarItemList extends StatelessWidget {
  const CarItemList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
        itemCount: 10,
        separatorBuilder: (_, int index) {
          return const SizedBox(
            height: 16,
          );
        },
        itemBuilder: (context, index) {
          return const CarItem();
        });
  }
}
