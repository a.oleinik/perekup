import 'package:perekup/models/model.dart';

class Brand {
  final String name;

  final String logo;

  final List<Model> models;

  Brand({required this.name, required this.logo, required this.models});
}
