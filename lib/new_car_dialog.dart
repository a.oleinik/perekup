import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:perekup/hive/car.dart';

class NewCarDialog extends StatefulWidget {
  const NewCarDialog({Key? key}) : super(key: key);

  @override
  _NewCarDialogState createState() => _NewCarDialogState();
}

class _NewCarDialogState extends State<NewCarDialog> {
  TextEditingController controller = TextEditingController();

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: const Text('Create To-Do Entry'),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const SizedBox(height: 10),
          TextField(
            decoration: const InputDecoration(
              border: UnderlineInputBorder(),
              hintText: 'Enter a task',
            ),
            controller: controller,
          ),
          const SizedBox(height: 10),
        ],
      ),
      actions: <Widget>[
        ElevatedButton(
          child: const Text('Cancel'),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        ElevatedButton(
          child: const Text('Add'),
          onPressed: () {
            if (controller.text.isNotEmpty) {
              var car = Car()
                ..model = controller.text
                ..createdDate = DateTime.now()
                ..isSold = false
                ..makeYear = 2020
                ..price = 123123.31;
              Hive.box<Car>('cars').add(car);
            }
            Navigator.of(context).pop();
          },
        ),
      ],
    );
  }
}
